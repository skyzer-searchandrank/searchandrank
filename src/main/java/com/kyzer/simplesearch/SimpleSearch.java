package com.kyzer.simplesearch;

import com.kyzer.simplesearch.index.Indexer;
import com.kyzer.simplesearch.model.Index;
import com.kyzer.simplesearch.model.ScoredIndex;
import com.kyzer.simplesearch.search.SearchIndex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.file.Files;
import java.util.*;

public class SimpleSearch {

    static int EXIT_STATUS_PARAMETER_NOT_UNDERSTOOD = 4;

    private static final Logger logger = LoggerFactory.getLogger(SimpleSearch.class);

    public static void main(String args[]) throws ArrayIndexOutOfBoundsException {

        File path = null;

        if (args.length == 1) {
            if (new File(args[0]).isDirectory() || new File(args[0]).isFile()) {
                path = new File(args[0]);
            } else {
                logger.error("Exit application.");
                System.err.println("Directory or File is not valid please ensure permission and path are correct");
                System.exit(EXIT_STATUS_PARAMETER_NOT_UNDERSTOOD);
            }
        } else {
            logger.error("Exit application.");
            System.err.println("Missing parameter or too many parameters please add only directory location of text files");
            System.exit(EXIT_STATUS_PARAMETER_NOT_UNDERSTOOD);
        }

        List<Index> allIndices = createAllIndices(path);
        logger.info(allIndices.size() + " files indexed");

        String input = null;
        Scanner scanner = new Scanner(System.in);
        String searchCommand = "";

        while (!searchCommand.equals(":quit")) {
            System.out.print("search>");
            searchCommand = scanner.nextLine();
            if (searchCommand.equals(":quit")) {
                break;
            } else {
                List<ScoredIndex> scoredIndices = generateScoredAndRankedIndices(allIndices, searchCommand);
                scoredIndices.forEach(scoredIndex -> {
                    System.out.println(scoredIndex.getIndexName() + "|"
                            + scoredIndex.getPercentMatch() * 100 + "%|"
                            + scoredIndex.getTermFrequency());
                });
            }
        }

        System.exit(EXIT_STATUS_PARAMETER_NOT_UNDERSTOOD);

    }

    public static List<Index> createAllIndices(File path) {
        List<Index> indices = new ArrayList<Index>();
        List<File> filesList = getListOfFilesRecursively(path);
        filesList.stream().forEach(file -> indices.add(getSingleIndexFromFile(file)));
        return indices;
    }

    public static List<File> getListOfFilesRecursively(File directory) {
        List<File> filesList = new ArrayList<File>();

        for (File dirChild : directory.listFiles()) {
            // Iterate all file sub directories recursively
            if (dirChild.isDirectory()) {
                //recursive to move through child directories
                filesList.addAll(getListOfFilesRecursively(dirChild));
            } else {
                try {
                    // Check if the files is a soft link (aka symlink)
                    boolean isSymLink = Files.isSymbolicLink(dirChild.toPath());
                    if (isSymLink) {
                        dirChild = Files.readSymbolicLink(dirChild.toPath()).toRealPath().toFile();
                    }
                    // Add path to list
                    filesList.add(dirChild);
                    logger.info(dirChild.toString());
                } catch (Exception e) {
                    logger.error("Error: Unable to read file: " + dirChild.getAbsolutePath());
                }
            }
        }
        return filesList;
    }

    public static Index getSingleIndexFromFile(File path) {

        Indexer indexer = new Indexer(new Index(path.toString()));
        // Get the list of files from the directory
        Scanner s = null;
        try {
            s = new Scanner(new BufferedReader(new FileReader(path)));

            while (s.hasNext()) {
                String term = s.next();
                indexer.addTermToIndex(term);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            if (s != null) {
                s.close();
            }
        }
        return indexer.getCurrentIndex();
    }

    public static List<ScoredIndex> generateScoredAndRankedIndices(List<Index> indices, String input) {
        SearchIndex indexSearcher = new SearchIndex();

        List<ScoredIndex> scoredIndices = new ArrayList<ScoredIndex>();
        indices.stream().forEach(indx -> scoredIndices.add(indexSearcher.searchAndScoreIndex(indx, input)));

        // uses built in in sorting for java as its optimized and leverage
        // comparable in data model
        Collections.sort(scoredIndices);

        return scoredIndices;
    }


}

package com.kyzer.simplesearch.model;

public class ScoredIndex extends Index implements Comparable<ScoredIndex> {
    Double percentMatch;
    Integer termFrequency;

    public ScoredIndex(Index indx) {
        super(indx.getIndexName());
        this.setIndexTermMap(indx.getIndexTermMap());
    }

    public Double getPercentMatch() {
        return percentMatch;
    }

    public void setPercentMatch(Double percentMatch) {
        this.percentMatch = percentMatch;
    }

    public Integer getTermFrequency() {
        return termFrequency;
    }

    public void setTermFrequency(Integer termFrequency) {
        this.termFrequency = termFrequency;
    }

    @Override
    public int compareTo(ScoredIndex scoredIndex) {
        // First by percent then by term frequency
        int scoresComp = scoredIndex.getPercentMatch().compareTo(this.getPercentMatch());
        return ((scoresComp == 0) ? scoredIndex.getTermFrequency().compareTo(this.getTermFrequency()) : scoresComp);
    }

}

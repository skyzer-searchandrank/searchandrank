package com.kyzer.simplesearch.model;

import java.util.HashMap;

public class Index{

    // This will be the absolute file location...for now.
    private String indexName;

    public Index(String indexName) {
        this.indexName = indexName;
    }

    public String getIndexName() {
        return indexName;
    }

    public void setIndexName(String indexName) {
        this.indexName = indexName;
    }

    // Key is term, value is count of term
    private HashMap<String, Integer> indexTermMap = new HashMap<String, Integer>();

    public HashMap<String, Integer> getIndexTermMap() {
        return indexTermMap;
    }

    public void setIndexTermMap(HashMap<String, Integer> indexTermMap) {
        this.indexTermMap = indexTermMap;
    }

}

package com.kyzer.simplesearch.index;

import com.kyzer.simplesearch.model.Index;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;

// The scope of this class is to manipulate the index object and perform
// the logic required to update
public class Indexer {

    private static final Logger logger = LoggerFactory.getLogger(Indexer.class);

    public Index indx;

    // Must force the initialization with an index
    public Indexer(Index indx) {
        this.indx = indx;
    }

    public void addTermToIndex(String term) {
        //logic to add a term to the existing index
        // Find the term in the hashmap
        String normalizedTerm = normalizeTerm(term);
        HashMap<String, Integer> indexTermMap = this.indx.getIndexTermMap();

        if (indexTermMap.containsKey(normalizedTerm)) {
            indexTermMap.put(normalizedTerm, (indexTermMap.get(normalizedTerm) + 1));
        } else {
            indx.getIndexTermMap().put(normalizedTerm, 1);
        }
    }

    public Index getCurrentIndex() {
        return this.indx;
    }

    private String normalizeTerm(String term) {
        // remove all special characters and change to lower case
        String normalizedTerm = term.replaceAll("[^a-zA-Z ]", "").toLowerCase();
        return normalizedTerm;
    }

}

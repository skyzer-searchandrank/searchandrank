package com.kyzer.simplesearch.search;

import com.kyzer.simplesearch.model.Index;
import com.kyzer.simplesearch.model.ScoredIndex;

import java.util.*;

public class SearchIndex {

    public Set<String> tokenizeInput(String input) {
        Set<String> tokenSet = new HashSet<String>();
        StringTokenizer tokenizer = new StringTokenizer(input, " ");

        while (tokenizer.hasMoreTokens()) {
            tokenSet.add(tokenizer.nextToken()
                    .replaceAll("[^a-zA-Z ]", "")
                    .toLowerCase());
        }

        return tokenSet;
    }

    public ScoredIndex scoreIndex(Index indx, HashMap<String, Integer> termSearchResults,
                                  Set<String> tokenSet) {
        // size of the search term map divided by size of unique word list
        // also return the sum of the total word count if both have the same
        // percent we will use this as a secondary sort
        ScoredIndex scoredIndex = new ScoredIndex(indx);
        scoredIndex.setPercentMatch(Double.valueOf(termSearchResults.size()) / Double.valueOf(tokenSet.size()));

        int termSum = 0;
        for (Map.Entry<String, Integer> entry : termSearchResults.entrySet()) {
            termSum += entry.getValue();
        }
        scoredIndex.setTermFrequency(termSum);

        return scoredIndex;
    }

    public ScoredIndex searchAndScoreIndex(Index indx, String input) {

        HashMap<String, Integer> termSearchResults = new HashMap<String, Integer>();
        Set<String> tokenSet = tokenizeInput(input);

        Iterator<String> it = tokenSet.iterator();

        while (it.hasNext()) {
            // if value != 0
            String term = it.next();
            if (indx.getIndexTermMap().containsKey(term)) {
                termSearchResults.put(term, indx.getIndexTermMap().get(term));
            }

        }
        return scoreIndex(indx, termSearchResults, tokenSet);
    }

}

package com.kyzer.simplesearch.index;

import com.kyzer.simplesearch.model.Index;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class IndexerTest {
    Index indx = new Index("testindex");
    Indexer indxr = new Indexer(indx);


    @Before
    public void setUp() throws Exception {
        indxr.addTermToIndex("one");
        indxr.addTermToIndex("one");
        indxr.addTermToIndex("one");
        indxr.addTermToIndex("three");
        indxr.addTermToIndex("three");
        indxr.addTermToIndex("three");
        indxr.addTermToIndex("four");
    }

    @Test
    public void addTermToIndex() throws Exception {
        assertEquals(indx.getIndexTermMap().get("one").longValue(), 3);
        indxr.addTermToIndex("one");
        assertEquals(indx.getIndexTermMap().get("one").longValue(), 4);
        indxr.addTermToIndex("two");
        assertEquals(indx.getIndexTermMap().get("two").longValue(), 1);
        assertNotEquals(indx.getIndexTermMap().get("two").longValue(), 3);
    }

    @Test
    public void getCurrentIndex() throws Exception {
        assertEquals(this.indxr.getCurrentIndex(), indx);
    }

}
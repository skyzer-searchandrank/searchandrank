package com.kyzer.simplesearch.search;

import com.kyzer.simplesearch.model.Index;
import com.kyzer.simplesearch.model.ScoredIndex;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import static org.mockito.Mockito.mock;


import static org.junit.Assert.*;

public class SearchIndexTest {

    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testTokenizeInput() throws Exception {
        Set<String> testTokenSet = new HashSet<String>();
        testTokenSet.add("to");
        testTokenSet.add("be");
        testTokenSet.add("or");
        testTokenSet.add("not");

        String input = "To be, or not to be.";
        SearchIndex si = new SearchIndex();
        Set<String> tokenizedString = si.tokenizeInput(input);

        assertEquals(tokenizedString,testTokenSet);
    }

    @Test
    public void testSearchAndScoreIndex() throws Exception {
        Index indx = new Index("testIndex");
        HashMap<String, Integer> testTermMap = new HashMap<String, Integer>();
        testTermMap.put("to",2);
        testTermMap.put("be",2);
        testTermMap.put("or",1);
        testTermMap.put("not",1);
        indx.setIndexTermMap(testTermMap);

        SearchIndex si = new SearchIndex();
        String input = "To be, or not to be, that is the question";

        ScoredIndex scoredIndex = si.searchAndScoreIndex(indx,input);
        assertEquals(scoredIndex.getIndexName(), indx.getIndexName());
        assertEquals(scoredIndex.getIndexTermMap(),testTermMap);
        assertEquals(scoredIndex.getPercentMatch().doubleValue(), 0.5, 0);
        assertEquals(scoredIndex.getTermFrequency(), 6,0);
    }

}
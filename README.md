# README #

This application is designed to read from a specified directory recursively, 
build an index and then allow the user to search the index. It will then rank
the all the indexes according to the word input into the search prompt. 

### What is this repository for? ###

* Designed to host the Simple Search application and all related components

### How do I get set up? ###

1. `git clone {location of github}`
2. `mvn clean compile assembly:single`
3. `java -jar {local target path}/searchandrank-1.0-SNAPSHOT-jar-with-dependencies.jar {directory of text files}`

Use `:quit` from the `search>` prompt to exit and to search enter string of words in prompt.

### Sample output ###

Create executable Jar
`mvn clean compile assembly:single`

Run executable Jar
`java -jar searchandrank-1.0-SNAPSHOT-jar-with-dependencies.jar /searchandrank/src/test/resources`

Console output sample:

`2017-10-23 19:05:34 INFO  SimpleSearch:39 - 3 files indexed`

`search>to be or not to be`

`/src/test/resources/test2.txt|100.0%|182`

`/src/test/resources/test1.txt|100.0%|95`

`/src/test/resources/test3.txt|100.0%|75`


`search>apple`

`/src/test/resources/test3.txt|100.0%|27`

`/src/test/resources/test2.txt|100.0%|15`

`/src/test/resources/test1.txt|100.0%|14`

`search>:quit`

### Contribution guidelines ###

* This project uses CI/CD and trunk based development [https://trunkbaseddevelopment.com/](https://trunkbaseddevelopment.com/)
* The project is hosted under bitbucket with standard CI/CD pipeline
* Under MIT license See [https://en.wikipedia.org/wiki/MIT_License](https://en.wikipedia.org/wiki/MIT_License) 

### Who do I talk to? ###

* [shawnkyzer@gmail.com](mailto:shawnkyzer@gmail.com)

